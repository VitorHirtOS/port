import './css/style.css'
import { Link } from 'react-router-dom'

export function Footer(){
    return(
        <>

        <footer>
            <div>
                <div>
                    <div>
                    <h3> <Link to="/"> Vitor Mateus Hirt</Link></h3>
                    </div>
                </div>
                <div>
                    Todos os direitos reservados ao proprietário do site &copy;
                </div>
            </div>
        </footer>    

        </>
    )
}