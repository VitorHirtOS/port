import './css/style.css'

const Faculdade = () => {
    return(
        <>
        <div>
            <div>
                <div id="Faculdade" className='skills'>
                    <div>
                        <h1>Faculdade</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills C'>
                    <div>
                        <div>
                        <ion-icon name="terminal-outline"></ion-icon>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalC">
                                        C
                                       </button>
                                       <div className="modal fade" id="exampleModalC" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">C</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               A linguagem de programação C é uma linguagem de programação de propósito geral criada por Dennis Ritchie na década de 1970. 
                                               É uma das linguagens de programação mais antigas e amplamente utilizadas, sendo conhecida por sua eficiência e baixo nível de abstração.
                                               </div>
                                               <div>
                                                   <progress value="20" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-solid fa-microchip"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalC++">
                                        C++
                                       </button>
                                       <div className="modal fade" id="exampleModalC++" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">C++</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               C++ é uma linguagem de programação de uso geral que foi desenvolvida por Bjarne Stroustrup na década de 1980. 
                                               Ela é uma extensão da linguagem C e possui recursos adicionais que permitem a programação orientada a objetos, além de suportar a programação procedural.
                                               </div>
                                               <div>
                                                   <progress value="20" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Faculdade