import './css/style.css'
export const Ageis = () => {
    return(
        <>
            <div>
                <div>
                    <div id="Organização" className='skills'>
                        <div>
                            <h1>Organização</h1>
                        </div>
                        <div>

                        </div>
                        <div>

                        </div>
                    </div>
                    <div className='cardSkills'>
                        <div>
                            <div>
                                <i className="fa-brands fa-trello"></i>
                            </div>
                            <div className='modalBox'>

                                <button type="button" className="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#exampleModalTrello">
                                    Trello
                                </button>
                                <div className="modal fade" id="exampleModalTrello" tabIndex="-1"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h1 className="modal-title fs-5" id="exampleModalLabel">Trello</h1>
                                                <button type="button" className="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                            </div>
                                            <div className="modal-body">
                                                <div>
                                                    O Trello é uma plataforma online que permite a organização e o gerenciamento de projetos de forma colaborativa. É uma ferramenta de gestão de tarefas baseada em quadros,
                                                    onde os usuários podem criar listas e cartões para representar tarefas, atividades ou ideias. O Trello usa o conceito de Kanban, que é um sistema visual que permite
                                                    que as equipes visualizem o progresso de um projeto em diferentes estágios.
                                                </div>
                                                <div>
                                                    <progress value="50" max="100"></progress>
                                                    <span>Intermediário / Avançado</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}