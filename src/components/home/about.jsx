import { Footer } from '../footer'
import { Header } from '../header'
import './css/style.css'
import {event1, event2} from './js'

export const About = () => {
    return(
        <>
        <Header/>
            <div className='aboutInfo' id="about">
                <div className='info'>
                        <div className="VitorPhotos">
                        
                    </div>
                    <div className='information'>
                        <div>
                            <h1>Informações Pessoais</h1>
                            <div>
                                    <p>Nome: Vitor Mateus Hirt</p>
                                    <p>Nascionalidade: Brasileiro</p>
                                    <p>Endereço: Novo Hamburgo - RS</p>
                                    <p>Idioma: Português Fluente</p>
                            </div>
                            <div>
                                    <p>Idade: 22 anos</p>
                                    <p>Freelance: Disponível</p>
                                    <p>Email: vitorhirt.hirt@outlook.com ou zorinos.masterzorin@outlook.com</p>
                            </div>
                            <div className='CV'>
                                <a href="./static/Curriculum.pdf" download>
                                    <span>Download CV</span> 
                                    <span>
                                        <i className="fa-solid fa-download"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div className='VitorCertification'>
                            <div>
                                <span onClick={event1}>
                                    <span className='number'>
                                        4+
                                    </span>
                                    <span>
                                        Projetos em andamentos
                                    </span>
                                </span>
                                <div id='project_progress'>
                                    <div>
                                        <h1>Projetos</h1>
                                        <i id='closeProject' className="fa fa-circle-xmark"></i>
                                    </div>
                                    <div className='scrollProject'>
                                        <div className='southbike'>
                                            <span>
                                            <div>

                                            </div>
                                            <a href="#">Veja mais...</a> 
                                            </span>
     
                                        </div>
                                        <div className='coinsbank'>
                                            <span>
                                            <div>

                                            </div>
                                            <a href="#">Veja mais...</a> 
                                            </span>
                                        </div>
                                    </div>
                                    <div className='scrollProject'>
                                    <div className='app'>
                                            <span>
                                            <div>

                                            </div>
                                            <a href="#">Veja mais...</a> 
                                            </span>
                                        </div>
                                        <div className='ecommerce'>
                                            <span>
                                            <div>

                                            </div>
                                            <a href="#">Veja mais...</a> 
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div >
                                <span onClick={event2}>
                                    <span className='number'>
                                        4º 
                                    </span>
                                    <span>
                                        Semestre de Análise e Desenvolvimento de Sistemas
                                    </span>   
                                </span>

                                <div id="faculty">
                                    <div>
                                        <i className="fa-solid fa-graduation-cap"></i>
                                        <i id='closeFaculty' className="fa fa-circle-xmark"></i>
                                    </div>
                                    <div>
                                        <h1>Centro Universitário Leonardo Da Vinci - Uniasselvi</h1>
                                    </div>
                                    <div>
                                        <h3>4º - Semestre da faculdade - Modalidade flex</h3>
                                    </div>
                                </div>
                            </div>
                            <div >
                                <span>
                                    <a href="https://www.linkedin.com/in/vitor-hirt-566292252/details/certifications/" target="_blank" rel="noopener noreferrer">
                                        <span className='number'>
                                                10
                                            </span>
                                            <span>
                                                Certificação de cursos na área
                                            </span> 
                                    </a>

                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className='experience'>                        
                    <div>
                        <li>Southbike</li>
                        <div className='industry'>
                            <div>
                                <li>Cargo: Desenvolvimento Web - Aprendiz</li>
                                <li>Empresa: Southbike</li>
                                <li>Periodo: Março de 2022 - Presente</li>
                                <li>
                                Tive o papel principal no desenvolvimento da interface de usuário UX | UI da empresa Southbike, construindo uma aplicação intuitva e de fácil manuseio por parte dos clientes.
                                </li>
                                <li>Fui o responsável por dar o inicio no projeto desktop ERP da Southbike, sendo o desenvolvedor de interface da aplicação.</li>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        <Footer/>
        </>
    )
}
