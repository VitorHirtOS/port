import './css/style.css'

const Back_End = () => {
    return(
        <>
        <div>
            <div>
                <div id="BackEnd" className='skills'>
                    <div>
                        <h1>Back-End</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                        <ion-icon name="logo-nodejs"></ion-icon>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalNode">
                                       NodeJS
                                       </button>
                                       <div className="modal fade" id="exampleModalNode" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">NodeJS</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Node.js é um ambiente de tempo de execução JavaScript multiplataforma e de código aberto criado no mecanismo JavaScript V8 do Chrome. 
                                               Ele permite que você execute o código JavaScript fora de um navegador da Web, possibilitando a execução do JavaScript no lado do servidor. 
                                               O Node.js fornece um modelo de E/S sem bloqueio e orientado a eventos, o que o torna leve e eficiente para lidar com operações simultâneas.
                                               </div>
                                               <div>
                                                   <progress value="50" max="100"></progress><span>Básico / Intermediário</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-brands fa-laravel"></i>
                        </div>
                        <div className='modalBox'>
                                       
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalLaravel">
                        Laravel
                        </button>
                        <div className="modal fade" id="exampleModalLaravel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Laravel</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                            </div>
                            <div className="modal-body">
                                <div>
                                O Laravel é um dos frameworks de desenvolvimento web mais populares e amplamente utilizados em PHP. 
                                Ele foi criado por Taylor Otwell e lançado em 2011, com o objetivo de simplificar o desenvolvimento de aplicativos web robustos e escaláveis.
                                </div>
                                <div>
                                    <progress value="20" max="100"></progress><span>Básico</span> 
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                            </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-brands fa-phoenix-framework"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalDjango">
                                        Django
                                       </button>
                                       <div className="modal fade" id="exampleModalDjango" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Django</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               Django é um popular framework web Python de alto nível que segue o padrão arquitetônico Model-View-Controller (MVC). 
                                               Ele fornece um conjunto de ferramentas e bibliotecas que ajudam os desenvolvedores a criar aplicativos da Web com rapidez e eficiência.
                                               </div>
                                               <div>
                                                   <progress value="40" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Back_End