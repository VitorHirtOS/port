export default function Program(){
    return(
        <>
        <div id="skills">
        <div>
            <div className='skills'>
                <div>
                    <h1>Linguagens de Programação</h1>
                </div>
                <div>

                </div>
                <div>

                </div>
            </div>
            <div className='cardSkills'>
                <div>
                    <div>
                    <ion-icon name="logo-python"></ion-icon>
                    </div>
                    <div className='modalBox'>
                                   
                <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalPython">
                    Python
                </button>
                <div className="modal fade" id="exampleModalPython" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Python</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                    </div>
                    <div className="modal-body">
                        <div>
                        Python é uma linguagem de programação popular conhecida por sua simplicidade e legibilidade. É amplamente utilizado para diversos fins, como desenvolvimento web, 
                        análise de dados, inteligência artificial, computação científica e muito mais. Se você tiver alguma dúvida específica sobre Python ou precisar de ajuda com uma tarefa 
                        específica, entre em contato e farei o possível para ajudá-lo.
                        </div>
                        <div>
                            <progress value="40" max="100"></progress><span>Básico / Intermediário</span> 
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                    </div>
                </div>
                <div>
                    <div>
                        <ion-icon name="logo-javascript"></ion-icon>
                    </div>
                    <div className='modalBox'>
                                   
                <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalJavascript">
                Javascript
                </button>
                <div className="modal fade" id="exampleModalJavascript" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Javascript</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                    </div>
                    <div className="modal-body">
                        <div>
                        JavaScript é uma linguagem de programação de alto nível usada principalmente para desenvolvimento da Web do lado do cliente. 
                            Ele permite que os desenvolvedores criem elementos interativos e dinâmicos em páginas da web. 
                        </div>
                        <div>
                                <progress value="70" max="100"></progress><span>Intermediário / Avançado</span> 
                            </div>
                    </div>
                    </div>
                </div>
                </div>
                    </div>
                </div>
                <div>
                    <div>
                    <i className="fa-brands fa-staylinked"></i>
                    </div>
                    <div className='modalBox'>
                                   
                <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalTypescript">
                Typescript
                </button>
                <div className="modal fade" id="exampleModalTypescript" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Typescript</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                    </div>
                    <div className="modal-body">
                        <div>
                        O Typescript é uma linguagem de programação que foi desenvolvida como uma extensão do JavaScript. Ela adiciona recursos e funcionalidades adicionais ao JavaScript, tornando-o mais poderoso e seguro.
                                Uma das principais características do TypeScript é a tipagem estática. Isso significa que, ao contrário do JavaScript, onde as variáveis podem ter qualquer tipo de dado, no TypeScript é possível definir o tipo das variáveis de 
                                forma explícita. Isso ajuda a evitar erros comuns de digitação e fornece um melhor suporte para o desenvolvedor durante a escrita do código.
                        </div>
                        <div>
                                    <progress value="20" max="100"></progress><span>Básico</span> 
                                </div>
                    </div>
                    </div>
                </div>
                </div>
                    </div>
                </div>
                <div>
                    <div>
                       <i class="fa-brands fa-microsoft"></i>
                    </div>

                    <div className='modalBox'>
                                   
                    <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalSass">
                    cSharp
                    </button>
                    <div className="modal fade" id="exampleModalSass" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">C#</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                        </div>
                        <div className="modal-body">
                            <div>
                            C# (pronuncia-se "C Sharp") é uma linguagem de programação moderna, orientada a objetos e desenvolvida pela Microsoft. 
                            Ela foi lançada em 2000 como parte da plataforma .NET e tem se tornado cada vez mais popular ao longo dos anos.
                            </div>
                            <div>
                                <progress value="20" max="100"></progress><span>Básico</span> 
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                        </div>
                </div>
                <div>
                    <div>
                    <i className="fa-brands fa-php"></i>
                    </div>

                    <div className='modalBox'>
                                   
                    <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalPhp">
                    Php
                    </button>
                    <div className="modal fade" id="exampleModalPhp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">Php</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                        </div>
                        <div className="modal-body">
                        <div>
                                PHP é uma popular linguagem de script do lado do servidor usada para desenvolvimento web. Foi criado por Rasmus Lerdorf em meados da década de 1990 e significa "PHP: Hypertext Preprocessor". 
                                PHP é projetado para gerar páginas web dinâmicas e interagir com bancos de dados e outras tecnologias do lado do servidor.
                                </div>
                                <div>
                                    <progress value="40" max="100"></progress><span>Básico / Intermediário</span> 
                                </div>
                        </div>
                        </div>
                    </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
        </>
    )
}