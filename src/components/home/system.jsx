import './css/style.css'

const System = () => {
    return(
        <>
        <div>
            <div>
                <div id="SistemasOperacionais" className='skills'>
                    <div>
                        <h1>Sistemas Operacionais</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                        <ion-icon name="logo-windows"></ion-icon>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalWindow">
                                        Windows
                                       </button>
                                       <div className="modal fade" id="exampleModalWindow" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Windows</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Windows é um sistema operacional desenvolvido pela Microsoft. Ele é amplamente utilizado em computadores pessoais, laptops, tablets e outros dispositivos. 
                                               O Windows fornece uma interface gráfica amigável que permite aos usuários interagir com o computador de forma intuitiva.
                                               </div>
                                               <div>
                                                   <progress value="85" max="100"></progress><span>Intermediário / Avançado</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                        <i className="fa-brands fa-linux"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalLinux">
                                        Linux
                                       </button>
                                       <div className="modal fade" id="exampleModalLinux" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Linux</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Linux é um sistema operacional de código aberto baseado em Unix. Diferentemente do Windows, que é desenvolvido pela Microsoft, 
                                               o Linux é criado colaborativamente por uma comunidade global de desenvolvedores. Ele é conhecido por sua estabilidade, segurança e flexibilidade.
                                               </div>
                                               <div>
                                                   <progress value="70" max="100"></progress><span>Intermediário</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default System