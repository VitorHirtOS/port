import './css/style.css'
import Southbike from './img/southbike.jpg'
import Coins from './img/coinsbank.jpg'
import Desktop from './img/application.jpg'
import Ecommerce from './img/ecommerce.jpg'

const CardShow = () => {
    return(
        <>
        <div id="carouselExampleIndicators" className="carousel slide">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active">
    <img src={Southbike} className="d-block w-100 imgProject" alt="..."/>
      <a href="/southbike"><h1>Southbike</h1></a>
    </div>
    <div className="carousel-item">
      <img src={Coins} className="d-block w-100 imgProject" alt="..."/>
      <a href="/coinsbank"><h1>CoinsBank</h1></a>
    </div>
    <div className="carousel-item">
      <img src={Desktop} className="d-block w-100 imgProject" alt="..."/>
      <a href="/aplicativo_southbike"><h1>Aplicativo Southbike</h1></a>
    </div>
    <div className="carousel-item">
      <img src={Ecommerce} className="d-block w-100 imgProject" alt="..."/>
      <a href="/ecommerce"><h1>Ecommerce</h1></a>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>
        </>
    )
}

export default CardShow