import './css/style.css'

const Libs = () => {
    return(
        <>
        <div>
            <div>
                <div id="ProdutividadeLibs" className='skills'>
                    <div>
                        <h1>Produtividade Libs</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                            <i className="fa-brands fa-bootstrap"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalBootstrap">
                                       Bootstrap
                                       </button>
                                       <div className="modal fade" id="exampleModalBootstrap" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Bootstrap</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Bootstrap é uma estrutura popular de código aberto usada para criar sites e aplicativos da Web responsivos e voltados para dispositivos móveis. 
                                               Ele fornece uma coleção de componentes CSS e JavaScript que simplificam o processo de design e desenvolvimento de interfaces de usuário.
                                               </div>
                                               <div>
                                                   <progress value="80" max="100"></progress><span>Avançado</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <i className="bi bi-tsunami"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalTailwind">
                                       Tailwind
                                       </button>
                                       <div className="modal fade" id="exampleModalTailwind" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Tailwind</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Tailwind CSS é uma estrutura CSS popular de primeiro uso usada para criar interfaces de usuário. 
                                               Ele fornece um conjunto de classes CSS pré-construídas que podem ser facilmente aplicadas a elementos HTML para estilizá-los sem escrever CSS personalizado. 
                                               Tailwind CSS visa fornecer uma abordagem de estilo altamente personalizável e flexível, mantendo a simplicidade.
                                               </div>
                                               <div>
                                                   <progress value="60" max="100"></progress><span>Básico / Intermediário</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                        <i className="fa-solid fa-feather-pointed"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalBulma">
                                       Bulma
                                       </button>
                                       <div className="modal fade" id="exampleModalBulma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Bulma</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               Bulma é uma popular estrutura CSS de código aberto que fornece uma ampla variedade de componentes pré-estilizados e um sistema de grade responsivo. 
                                               O objetivo é simplificar o desenvolvimento da Web, oferecendo uma base CSS limpa e moderna. Aqui está uma visão geral do uso do Bulma CSS:
                                               </div>
                                               <div>
                                                   <progress value="60" max="100"></progress><span>Básico / Intermediário</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Libs