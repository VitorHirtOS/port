import './css/style.css'

const ResolucaoProblemas = () => {
    return(
        <>
        <div>
            <div>
                <div id="ResoluçãoDeProblemas" className='skills'>
                    <div>
                        <h1>Alternativas para a Resolução de Problemas</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                        <i className="fa-brands fa-stack-overflow"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalStack">
                                        StackOverflow
                                       </button>
                                       <div className="modal fade" id="exampleModalStack" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">StackOverflow</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O Stack Overflow é uma plataforma online popularmente conhecida por sua vasta comunidade de desenvolvedores e entusiastas de programação. 
                                               Fundado em 2008 por Joel Spolsky e Jeff Atwood, o Stack Overflow tornou-se um recurso indispensável para programadores em todo o mundo.
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-brands fa-slack"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalChatGpt">
                                        ChatGPT
                                       </button>
                                       <div className="modal fade" id="exampleModalChatGpt" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">ChatGPT</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O ChatGPT é um modelo de linguagem desenvolvido pela OpenAI, baseado na arquitetura GPT-3.5. Ele foi treinado com uma vasta quantidade de dados linguísticos 
                                               para gerar respostas coerentes e relevantes em uma conversa. O GPT significa "Transformador Gerador Pré-treinado" e é uma das mais avançadas tecnologias de processamento 
                                               de linguagem natural disponíveis atualmente.
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                        <i className="fa-solid fa-book"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalDocs">
                                        Docs
                                       </button>
                                       <div className="modal fade" id="exampleModalDocs" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Docs</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               A documentação das linguagens de programação é uma ferramenta essencial para os desenvolvedores. Ela fornece informações detalhadas sobre a sintaxe, 
                                               recursos, bibliotecas e funcionalidades da linguagem. O objetivo da documentação é ajudar os programadores a entenderem como usar corretamente a linguagem e a 
                                               aproveitar ao máximo suas capacidades.
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default ResolucaoProblemas