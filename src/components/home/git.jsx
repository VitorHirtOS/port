import './css/style.css'

const Git = () => {
    return(
        <>
        <div>
            <div>
                <div id="VersionamentoGit" className='skills'>
                    <div>
                        <h1>Versionamente de Código</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                            <i className="fa-brands fa-git-alt"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalGit">
                                       Git / Github
                                       </button>
                                       <div className="modal fade" id="exampleModalGit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Git / Github</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               Git é um sistema de controle de versão distribuído amplamente utilizado para o gerenciamento de código-fonte e colaboração de desenvolvimento 
                                               de software. Ele foi desenvolvido originalmente por Linus Torvalds em 2005 para o desenvolvimento do kernel do Linux, mas agora é utilizado 
                                               em projetos de software de todos os tipos.
                                               </div>
                                               <div>
                                                   <progress value="70" max="100"></progress><span>Intermediário</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Git