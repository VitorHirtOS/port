import './css/style.css'

const SQL = () => {
    return(
        <>
        <div>
            <div>
                <div id="BancoDeDados" className='skills'>
                    <div>
                        <h1>Banco de Dados</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                            <i className="fa-solid fa-database"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalMysql">
                                        Mysql
                                       </button>
                                       <div className="modal fade" id="exampleModalMysql" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Mysql</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O MySQL é um sistema de gerenciamento de banco de dados relacional (RDBMS) de código aberto amplamente usado para gerenciar e organizar dados estruturados. 
                                               Ele é desenvolvido e suportado pela Oracle Corporation. 
                                               O MySQL é um dos bancos de dados mais populares usados ​​em aplicativos da Web e é conhecido por seu desempenho, escalabilidade e facilidade de uso.
                                               </div>
                                               <div>
                                                   <progress value="30" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-solid fa-feather"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalMongodb">
                                        MongoDB
                                       </button>
                                       <div className="modal fade" id="exampleModalMongodb" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">MongoDB</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O MongoDB é um popular banco de dados NoSQL orientado a documentos de código aberto que fornece alta escalabilidade, flexibilidade e desempenho. Ele foi desenvolvido pela MongoDB Inc. 
                                               e lançado inicialmente em 2009. O MongoDB foi projetado para lidar com grandes quantidades de dados e oferece um esquema dinâmico, o que significa que você pode armazenar e recuperar dados de maneira flexível e sem esquema.
                                               </div>
                                               <div>
                                                   <progress value="30" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                        <i className="fa-solid fa-server"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalSqlServer">
                                        Sql Server
                                       </button>
                                       <div className="modal fade" id="exampleModalSqlServer" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Sql Server</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               O SQL Server é um sistema de gerenciamento de banco de dados relacional desenvolvido pela Microsoft. 
                                               Ele fornece um ambiente seguro e escalável para armazenar, gerenciar e acessar dados
                                               </div>
                                               <div>
                                                   <progress value="30" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default SQL