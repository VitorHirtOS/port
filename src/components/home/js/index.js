/*faculty
certificate
*/

export const event1 = () => {
    
    const project_progress = document.getElementById('project_progress')
    project_progress.classList.toggle('openProject_progress')

    document.getElementById('closeProject').addEventListener('click', function(){
        project_progress.classList.remove('openProject_progress')
    })

} 

export const event2 = () => {
    
    const faculty = document.getElementById('faculty')
    faculty.classList.toggle('openProject_progress')

    document.getElementById('closeFaculty').addEventListener('click', function(){
        faculty.classList.remove('openProject_progress')
    })

} 
