import './css/style.css'

const Design = () => {
    return(
        <>
        <div>
            <div>
                <div id="Design" className='skills'>
                    <div>
                        <h1>Design</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                            <ion-icon name="logo-figma"></ion-icon>
                        </div>
                        <div className='modalBox'>
                                       
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalFigma">
                        Figma
                        </button>
                        <div className="modal fade" id="exampleModalFigma" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Figma</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                            </div>
                            <div className="modal-body">
                                <div>
                                Figma é uma ferramenta popular de design e prototipagem baseada na Web usada por designers e equipes para criar interfaces de usuário, protótipos interativos e colaborar em projetos de design. Foi desenvolvido pela Figma Inc. 
                                e lançado em 2016. O Figma permite que vários usuários trabalhem em um projeto simultaneamente, tornando-o adequado para projetos colaborativos.
                                </div>
                                <div>
                                    <progress value="90" max="100"></progress><span>Avançado</span> 
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                            </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-solid fa-palette"></i>
                        </div>
                        <div className='modalBox'>
                                       
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalAdobe">
                        AdobeXD
                        </button>
                        <div className="modal fade" id="exampleModalAdobe" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">AdobeXD</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                            </div>
                            <div className="modal-body">
                                <div>
                                Adobe XD é uma poderosa ferramenta de design e prototipagem de interfaces de usuário. Desenvolvido pela Adobe Systems, 
                                o XD foi criado especificamente para designers de experiência do usuário (UX) e designers de interface do usuário (UI) 
                                criarem designs interativos e funcionais.
                                </div>
                                <div>
                                    <progress value="20" max="100"></progress><span>Básico</span> 
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Design