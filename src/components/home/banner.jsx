import './sass/style.sass'
import  { useEffect, useState } from 'react';


const Banner = () => {

  const [title, setTitle] = useState('');

  useEffect(() => {
    const textoArray = Array.from('Olá, me chamo Vitor Mateus Hirt. Sou desenvolvedor software.');
    let typedText = '';
    textoArray.forEach((letra, i) => {
      setTimeout(() => {
        typedText += letra;
        setTitle(typedText);
      }, 75 * i);
    });
  }, []);

    return(
        <>
        <div id="bannerId">
            <div>
                <div>
                <h1 id="title">{title}</h1>
                </div>
                <div>
                    <p>Minhas redes sociais de contatos:</p>
                    <span><a href="https://www.linkedin.com/in/vitor-hirt-566292252/" target="_blank" rel="noopener noreferrer"><ion-icon name="logo-linkedin"></ion-icon></a></span>
                    <span><a href="https://github.com/VitorHirtOS" target="_blank" rel="noopener noreferrer"><ion-icon name="logo-github"></ion-icon></a></span>
                    <span><a href="https://api.whatsapp.com/send?phone=5551980546291" target="_blank" rel="noopener noreferrer"><ion-icon name="logo-whatsapp"></ion-icon></a></span>
                </div>
                <div className="btn">
                    <a className="btnHref"  href="/about"><span>Minhas Experiências</span><i className="fa-solid fa-paper-plane"></i></a>
                </div>
            </div>
            <div className='logo2'>
                <span>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div>V</div>
                </span>
            </div>
        </div>
        </>
    )
}

export default Banner