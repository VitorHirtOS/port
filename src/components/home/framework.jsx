import './css/style.css'

const Frameworks = () => {
    return(
        <>
        <div>
            <div>
                <div id="Frameworks" className='skills'>
                    <div>
                        <h1>Frameworks</h1>
                    </div>
                    <div>

                    </div>
                    <div>

                    </div>
                </div>
                <div className='cardSkills'>
                    <div>
                        <div>
                            <i className="fa-brands fa-phoenix-framework"></i>
                        </div>
                        <div className='modalBox'>
                                       
                                       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalDjango">
                                        Django
                                       </button>
                                       <div className="modal fade" id="exampleModalDjango" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                       <div className="modal-dialog">
                                           <div className="modal-content">
                                           <div className="modal-header">
                                               <h1 className="modal-title fs-5" id="exampleModalLabel">Django</h1>
                                               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                                           </div>
                                           <div className="modal-body">
                                               <div>
                                               Django é um popular framework web Python de alto nível que segue o padrão arquitetônico Model-View-Controller (MVC). 
                                               Ele fornece um conjunto de ferramentas e bibliotecas que ajudam os desenvolvedores a criar aplicativos da Web com rapidez e eficiência.
                                               </div>
                                               <div>
                                                   <progress value="40" max="100"></progress><span>Básico</span> 
                                               </div>
                                           </div>
                                           </div>
                                       </div>
                                       </div>
                                           </div>
                    </div>
                    <div>
                        <div>
                            <ion-icon name="logo-electron"></ion-icon>
                        </div>
                        <div className='modalBox'>
                                       
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalElectronJs">
                        ElectronJs
                        </button>
                        <div className="modal fade" id="exampleModalElectronJs" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">ElectronJs</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                            </div>
                            <div className="modal-body">
                                <div>
                                Electron.js, muitas vezes referido simplesmente como Electron, é uma estrutura de código aberto que permite aos desenvolvedores 
                                criar aplicativos de desktop multiplataforma usando tecnologias da Web, como HTML, CSS e JavaScript. Foi criado pelo GitHub e lançado em 2013.
                                </div>
                                <div>
                                    <progress value="60" max="100"></progress><span>Básico / Intermediário</span> 
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                            </div>
                    </div>
                    <div>
                        <div>
                            <i className="fa-brands fa-laravel"></i>
                        </div>
                        <div className='modalBox'>
                                       
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalLaravel">
                        Laravel
                        </button>
                        <div className="modal fade" id="exampleModalLaravel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Laravel</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i className="bi bi-x-lg"></i></button>
                            </div>
                            <div className="modal-body">
                                <div>
                                O Laravel é um dos frameworks de desenvolvimento web mais populares e amplamente utilizados em PHP. 
                                Ele foi criado por Taylor Otwell e lançado em 2011, com o objetivo de simplificar o desenvolvimento de aplicativos web robustos e escaláveis.
                                </div>
                                <div>
                                    <progress value="20" max="100"></progress><span>Básico</span> 
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Frameworks