import { Route, BrowserRouter, Routes, Navigate } from 'react-router-dom'
import Home from './body'
import {Southbike} from './page/southbike'
import {Aplicativo} from './page//desktopSouthbike'
import {CoinsBank} from './page/coinsBank'
import {Ecommerce} from './page/ecommerce'
import {About} from './home/about'


export function RouterApp(){
    return(
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/southbike" element={<Southbike/>}/>
                <Route path="/aplicativo_southbike" element={<Aplicativo/>}/>
                <Route path="/coinsbank" element={<CoinsBank/>}/>
                <Route path="/ecommerce" element={<Ecommerce/>}/>
                <Route path="/about" element={<About/>}/>
                <Route path="*" element={<Navigate to="/" replace />} />  {/* Rota coringa reactjs */}
            </Routes>
        </BrowserRouter>
    )
} 