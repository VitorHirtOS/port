import './css/style.global.css'
import { MenuClick } from './js/index'
import Home from './shared/home'
import { Link } from 'react-router-dom'

export function Header(){

    const isHomePage = location.pathname === '/';

    return(
        <>
        <div id="headerId">
                <div className='headerNav'>
                    <div>
                    <button id="Menu" onClick={MenuClick}>
                        <i className="fas fa-bars"></i>
                    </button>
                        <div>
                        V
                        </div>
                     <Link to="/"><span>Vitor Mateus Hirt</span></Link>   
                    </div>
                    <div>
                        <nav>
                            <ul>
                            <a href="/"><li>Home</li></a>
                         {isHomePage ? (
                      <a href="#carouselExampleIndicators"><li>Projetos</li></a>
                      ):(
                        <Link to="/">Projetos</Link>
                        )}
                               {isHomePage ? (
                      <a href="#skills"><li>Habilidades</li></a>
                      ):(
                        <Link to="/">Habilidades</Link>
                        )}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <Home/>
        </>
    )
}