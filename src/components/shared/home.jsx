import { ancora } from '../js/index';

export default function Home(){
    
    const isHomePage = location.pathname === '/';

    return(
        <>
        <div id="headerMenu">
        <div className='headerNavigation'>
            <div>
                <div>
                    V
                </div>
                <span>Vitor Mateus Hirt</span>
            </div>
            <div>
                <nav>
                    <ul>
                      <a href="/"><li>Home</li></a>
                         {isHomePage ? (
                      <a href="#carouselExampleIndicators" onClick={ancora}><li>Projetos</li></a>
                      ):(
                        <a href="/">Projetos</a>
                        )}
                               {isHomePage ? (
                      <a href="#skills" onClick={ancora}><li>Habilidades</li></a>
                      ):(
                        <a href="/">Habilidades</a>
                        )}
                    </ul>
                </nav>
            </div>
        </div>
    </div>             
        </>
    )
}
