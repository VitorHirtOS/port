import './css/style.global.css'
import Back_End from './home/back-end'
import Banner from './home/banner'
import CardShow from './home/cardshow'
import Design from './home/design'
import Faculdade from './home/estudos'
import Frameworks from './home/framework'
import Front_End from './home/front-end'
import Git from './home/git'
import Libs from './home/libs'
import SQL from './home/sql'
import System from './home/system'
import {Ageis} from "./home/ageis.jsx"
import filter from './js/filter.js'
import ResolucaoProblemas from './home/resolucaoProblemas'
import { Header } from './header'
import { Footer } from './footer'
import Program from './home/Program'

export default function Body(){
    return(
        <>
        <Header/>
           <div className="cutBody">

            <button id="filter" className='filter' onClick={filter}>
                    <i className="fa-solid fa-house-chimney"></i>
            </button>
             <div className='filterBar'>
                     <span></span>
             </div>
                <div id="modalOpen" className="modalFilter">
                        <ul>
                                <a href="#bannerId"><li>Home</li></a>
                                <a href="#carouselExampleIndicators"><li>Projeto</li></a>
                                <a href="#skills"><li>Front-End</li></a>
                                <a href="#BackEnd"><li>Back-End</li></a>
                                <a href="#VersionamentoGit"><li>Versionamento Git</li></a>
                                <a href="#BancoDeDados"><li>Banco de Dados</li></a>
                                <a href="#Frameworks"><li>Frameworks</li></a>
                                <a href="#ProdutividadeLibs"><li>Produtividade Libs</li></a>
                                <a href="#Design"><li>Design</li></a>
                                <a href="#SistemasOperacionais"><li>Sistemas Operacionais</li></a>
                                <a href="#Faculdade"><li>Faculdade</li></a>                                
                                <a href="#ResoluçãoDeProblemas"><li>Resolução de Problemas</li></a>
                                <a href="#Organização"><li>Organização</li></a>

                        </ul>
                </div>
            <Banner/>
            <CardShow/>
            <Program/>
            <Front_End/>
            <Back_End/>
            <Git/>
            <SQL/>
            <Frameworks/>
            <Libs/>
            <Design/>
            <System/>
            <Faculdade/>
            <ResolucaoProblemas/>
            <Ageis/>
           </div>
           <Footer/>
        </>
    )
}
