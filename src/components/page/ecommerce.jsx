import { Footer } from '../footer'
import { Header } from '../header'
import './css/style.css'
import ecommerce from './img/ecommerce.png'

export const Ecommerce = () => {
    return(
        <>
        <Header/>
           <h1 className="h1">Projeto - Ecommerce</h1>
            <div className="imgBlock">
                <div>
                    <img src={ecommerce} alt="" />
                </div>
            </div>
            <div className='textProject'>
                <h3>Descrição</h3>
                <li>
                    Então, o projeto ecommerce, está sendo desenvolvido para englobar todos os meus aprendizados em programação, deste o front-end até o back-end, trabalhando com ligação com 
                    banco de dados, protocolos, api e MVC entre outros aspectos do desenvolvimento de ecommerce. Deste modo, colocando em prática toda a minha bagagem de conhecimentos sobre os 
                    assuntos, para chegar no meu objetivo que é construir uma aplicação completa de um sistema B2C.
                </li>
                <h3>Tecnologia</h3>
                <span>
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/HTML" target="_blank" rel="noopener noreferrer"><li>HTML <i className="fa-brands fa-html5"></i></li></a>
                  <a href="https://developer.mozilla.org/pt-BR/docs/Web/CSS" target="_blank" rel="noopener noreferrer"><li>CSS <i className="fa-brands fa-css3-alt"></i></li></a>  
                  <a href="https://sass-lang.com/" target="_blank" rel="noopener noreferrer"><li>SASS <i className="fa-brands fa-sass"></i></li></a> 
                   <a href="https://getbootstrap.com/" target="_blank" rel="noopener noreferrer"><li>BOOTSTRAP <i className="fa-brands fa-bootstrap"></i></li></a> 
                  <a href="https://tailwindcss.com/" target="_blank" rel="noopener noreferrer"><li>TAILWIND <i className="bi bi-tsunami"></i></li></a>  
                   <a href="https://bulma.io/" target="_blank" rel="noopener noreferrer"><li>BULMA   <i className="fa-solid fa-feather-pointed"></i></li></a> 
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript" target="_blank" rel="noopener noreferrer"><li>JAVASCRIPT <ion-icon name="logo-javascript"></ion-icon></li></a> 
                   <a href="https://www.python.org/" target="_blank" rel="noopener noreferrer"><li>PYTHON <ion-icon name="logo-python"></ion-icon></li></a> 
                   <a href="https://www.php.net/" target="_blank" rel="noopener noreferrer"><li>PHP <i className="fa-brands fa-php"></i></li></a> 
                   <a href="https://www.djangoproject.com/" target="_blank" rel="noopener noreferrer"><li>DJANGO <i className="fa-brands fa-phoenix-framework"></i></li></a> 
                  <a href="https://react.dev/" target="_blank" rel="noopener noreferrer"><li>REACTJS <ion-icon   ion-icon name="logo-react"></ion-icon></li></a>  
                  <a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer"><li>TYPESCRIPT      <i className="fa-brands fa-staylinked"></i></li></a>  
                  <a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer"><li>MYSQL  <i className="fa-solid fa-database"></i></li></a>  
                </span>
            </div>
            <div className='projectPorcente'>
                    <progress value="0" max="100"></progress><span>Não Comecei</span> 
                </div>
            <div className="btnProject">
                <div>
                    <a href="#">
                      <p>Ver Website</p>   
                      <i className="fa-solid fa-cloud"></i>
                    </a>
                </div>
                <div>
                    <a href="#">
                    <p>Ver o Projeto</p>   
                      <i className="fa-brands fa-github"></i>
                    </a>
                </div>
            </div>
            <Footer/>
        </>
    )
}
