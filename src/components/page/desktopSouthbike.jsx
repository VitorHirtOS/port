import { Footer } from '../footer'
import { Header } from '../header'
import './css/style.css'
import desktop from './img/Aplicativo.png'
import { Alert } from './js/alert'


export const Aplicativo = () => {
    return(
        <>
        <Header/>
           <h1 className="h1">Projeto - Aplicativo Southbike</h1>
            <div className="imgBlock">
                <div>
                    <img src={desktop} alt="" />
                </div>
            </div>
            <div className='textProject'>
                <h3>Descrição</h3>
                <li>
                    Similarmente, com o projeto Southbike, o aplicativo South, tem por objetivo desenvolver uma interface gráfica de um sistema ERP, para auxiliar a gestão organizacional
                    da empresa. Portanto, sendo um software para flexibilizar o trabalho do time administrativo e servindo como solução para os entraves encontrados no sistema utilizado
                    atualmente na empresa.
                </li>
                <h3>Tecnologia</h3>
                <span>
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/HTML" target="_blank" rel="noopener noreferrer"><li>HTML <i className="fa-brands fa-html5"></i></li></a>
                  <a href="https://developer.mozilla.org/pt-BR/docs/Web/CSS" target="_blank" rel="noopener noreferrer"><li>CSS <i className="fa-brands fa-css3-alt"></i></li></a>  
                  <a href="https://sass-lang.com/" target="_blank" rel="noopener noreferrer"> <li>SASS <i className="fa-brands fa-sass"></i></li></a> 
                   <a href="https://getbootstrap.com/" target="_blank" rel="noopener noreferrer"><li>BOOTSTRAP <i className="fa-brands fa-bootstrap"></i></li></a> 
                  <a href="https://tailwindcss.com/" target="_blank" rel="noopener noreferrer"><li>TAILWIND <i className="bi bi-tsunami"></i></li></a>  
                   <a href="https://bulma.io/" target="_blank" rel="noopener noreferrer"><li>BULMA   <i className="fa-solid fa-feather-pointed"></i></li></a> 
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript" target="_blank" rel="noopener noreferrer"><li>JAVASCRIPT <ion-icon name="logo-javascript"></ion-icon></li></a> 
                  <a href="https://react.dev/" target="_blank" rel="noopener noreferrer"><li>REACTJS <ion-icon   ion-icon name="logo-react"></ion-icon></li></a>  
                  <a href="https://www.electronjs.org/pt/" target="_blank" rel="noopener noreferrer"><li>ELECTRONJS <ion-icon name="logo-electron"></ion-icon></li></a>
                  <a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer"><li>TYPESCRIPT      <i className="fa-brands fa-staylinked"></i></li></a>  
                </span>
            </div>
            <div className='projectPorcente'>
                    <progress value="90" max="100"></progress><span>Em Andamento</span> 
                </div>
            <div id="alertProject">

            </div>
            <div className="btnProject">
                <div>
                    <a href="#">
                      <p>Ver Website</p>   
                      <i className="fa-solid fa-cloud"></i>
                    </a>
                </div>
                <div>
                    <a className='project' onClick={Alert}>
                    <p>Ver o Projeto</p>  
                      <i className="fa-brands fa-github"></i>
                    </a>
                </div>
            </div>
            <Footer/>
        </>
    )
}
