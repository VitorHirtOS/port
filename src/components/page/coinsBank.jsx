import { Footer } from '../footer'
import { Header } from '../header'
import './css/style.css'
import bank from './img/bank.svg'

export const CoinsBank = () => {
    return(
             <>
               <Header/>
           <h1 className="h1">Projeto - CoinsBank</h1>
            <div className="imgBlock">
                <div>
                    <img src={bank} alt="" />
                </div>
            </div>
            <div className='textProject'>
                <h3>Descrição</h3>
                <li>
                A priori, o desenvolvimento deste projeto tem por intensão melhorar o meu entendimento sobre as particularidades da tecnologia Javascript 
                com o cleave.js, sendo a sua estrutura lógica, como recursividade, métodos, DOM, dados primitivos etc. Portanto, o projeto tem foco em um sistema bancário, criando estruturas que realizam ligações com partes gráficas, como dashboard financeiro, deste modo, ampliando o meu repertório sobre esta conjuntura.    
                </li>
                <h3>Tecnologia</h3>
                <span className='spanTec'>
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/HTML" target="_blank" rel="noopener noreferrer"><li>HTML <i className="fa-brands fa-html5"></i></li></a>
                  <a href="https://developer.mozilla.org/pt-BR/docs/Web/CSS" target="_blank" rel="noopener noreferrer"><li>CSS <i className="fa-brands fa-css3-alt"></i></li></a>  
                  <a href="https://sass-lang.com/" target="_blank" rel="noopener noreferrer"> <li>SASS <i className="fa-brands fa-sass"></i></li></a> 
                   <a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript" target="_blank" rel="noopener noreferrer"><li>JAVASCRIPT <ion-icon name="logo-javascript"></ion-icon></li></a> 
                </span>
                <div className='projectPorcente'>
                    <progress value="50" max="100"></progress><span>Em Andamento</span> 
                </div>
            </div>
            <div className="btnProject">
                <div>
                    <a href="#">
                      <p>Ver Website</p>   
                      <i className="fa-solid fa-cloud"></i>
                    </a>
                </div>
                <div>
                    <a href="#">
                    <p>Ver o Projeto</p>   
                      <i className="fa-brands fa-github"></i>
                    </a>
                </div>
            </div>
            <Footer/>
        </>
    )
}
