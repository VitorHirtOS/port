export function Alert(){
    const Project = document.getElementById("alertProject")
    const span = document.createElement('span')
    span.innerText = 'Este projeto não é possivel ver o código fonte por motivos de políticas da empresa Southbike'
    span.classList.add('alertProject')
    Project.append(span)
    if(Project.getElementsByClassName('alertProject').length > 1){
        return span.remove()
    }
}