import './components/css/style.global.css'
import { RouterApp } from './components/router'
import '../src/components/home/css/style.css'

function App() {

  return (
    <>
          <div className='mainBody'>
              <RouterApp/>
          </div>
    </>
  )
}

export default App
